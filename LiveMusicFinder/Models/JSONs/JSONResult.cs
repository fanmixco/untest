﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LiveMusicFinder.Models.JSONs
{
    public class Coords
    {
        public double lat { get; set; }
        public double @long { get; set; }
    }

    public class Country
    {
        public string code { get; set; }
        public string name { get; set; }
    }

    public class City
    {
        public string id { get; set; }
        public string name { get; set; }
        public string state { get; set; }
        public string stateCode { get; set; }
        public Coords coords { get; set; }
        public Country country { get; set; }
    }

    public class Venue
    {
        public string id { get; set; }
        public string name { get; set; }
        public City city { get; set; }
        public string url { get; set; }
    }

    public class ArtistVenues
    {
        public string type { get; set; }
        public int itemsPerPage { get; set; }
        public int page { get; set; }
        public int total { get; set; }
        public List<Venue> venue { get; set; }
    }
}
