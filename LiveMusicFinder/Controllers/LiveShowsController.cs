﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using LiveMusicFinder.Data;
using LiveMusicFinder.Models;
using Microsoft.AspNetCore.Authorization;
using System.Net;
using Newtonsoft.Json;
using LiveMusicFinder.Models.JSONs;

namespace LiveMusicFinder.Controllers
{
    public class LiveShowsController : Controller
    {


        private readonly ApplicationDbContext _context;

        public LiveShowsController(ApplicationDbContext context)
        {
            _context = context;
        }

        public ActionResult GetArtistAvenues()
        {
            List<Venue> artistVenues;
            using (var wc = new WebClient())
            {
                wc.Headers["Accept"] = "application/json";
                wc.Headers["x-api-key"] = "b7pvUxYDmHwasVeKYRDZZSvo3OZ2HMtZMN6S";
                var json = wc.DownloadString("https://api.setlist.fm/rest/1.0/search/venues?cityName=Vienna&country=AT&p=1");
                artistVenues = JsonConvert.DeserializeObject<ArtistVenues>(json).venue;
            }

            return Json(artistVenues);
        }

        // GET: LiveShows

        [Authorize]
        public async Task<IActionResult> Index(string sortOrder)
        {
            ViewData["artistOrderParm"] = string.IsNullOrEmpty(sortOrder) ? "artist" : "";
            ViewData["venueOrderParm"] = string.IsNullOrEmpty(sortOrder) ? "venue" : "";
            ViewData["showDateOrderParm"] = string.IsNullOrEmpty(sortOrder) ? "showDate" : "";
            ViewData["enteredByOrderParm"] = string.IsNullOrEmpty(sortOrder) ? "enteredBy" : "";
            var liveShows = from s in _context.LiveShows
                            select s;
            switch (sortOrder)
            {
                case "artist":
                    liveShows = liveShows.OrderBy(s => s.Artist);
                    break;
                case "venue":
                    liveShows = liveShows.OrderBy(s => s.Venue);
                    break;
                case "showdate":
                    liveShows = liveShows.OrderByDescending(s => s.ShowDate);
                    break;
                case "enteredBy":
                    liveShows = liveShows.OrderByDescending(s => s.EnteredBy);
                    break;
                default:
                    liveShows = _context.LiveShows;
                    break;
            }
            return View(await liveShows.ToListAsync());
        }

        // GET: LiveShows/Details/5
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liveShow = await _context.LiveShows
                .FirstOrDefaultAsync(m => m.Id == id);
            if (liveShow == null)
            {
                return NotFound();
            }

            return View(liveShow);
        }

        // GET: LiveShows/Create
        [Authorize]
        public IActionResult Create()
        {
            var liveShow = new LiveShow();
            liveShow.EnteredBy = User.Identity.Name;
            return View(liveShow);
        }

        // POST: LiveShows/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,Artist,Venue,ShowDate,EnteredBy")] LiveShow liveShow)
        {
            if (ModelState.IsValid && !HasEvents(liveShow))
            {
                _context.Add(liveShow);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(liveShow);
        }

        // GET: LiveShows/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liveShow = await _context.LiveShows.FindAsync(id);
            if (liveShow == null)
            {
                return NotFound();
            }
            return View(liveShow);
        }

        private bool HasEvents(LiveShow liveShow)
        {
            return _context.LiveShows.Any(x => x.ShowDate.Date == liveShow.ShowDate.Date && x.Id != liveShow.Id);
        }

        // POST: LiveShows/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Artist,Venue,ShowDate,EnteredBy")] LiveShow liveShow)
        {
            var currentUser = User.Identity.Name;
            if (id != liveShow.Id)
            {
                return NotFound();
            }

            //validated only authorized users
            var existingShow = await _context.LiveShows.FindAsync(id);
            if (existingShow.EnteredBy != User.Identity.Name)
            {
                return Unauthorized();
            }

            if (ModelState.IsValid && !HasEvents(liveShow))
            {
                try
                {

                    existingShow.Artist = liveShow.Artist;
                    existingShow.Venue = liveShow.Venue;
                    existingShow.ShowDate = liveShow.ShowDate;
                    existingShow.EnteredBy = currentUser;

                    _context.Update(existingShow);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LiveShowExists(liveShow.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(liveShow);
        }

        // GET: LiveShows/Delete/5
        [Authorize]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var liveShow = await _context.LiveShows
                .FirstOrDefaultAsync(m => m.Id == id);
            if (liveShow == null)
            {
                return NotFound();
            }

            return View(liveShow);
        }

        // POST: LiveShows/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var liveShow = await _context.LiveShows.FirstOrDefaultAsync(x => x.Id == id);
            _context.LiveShows.Remove(liveShow);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LiveShowExists(int id)
        {
            return _context.LiveShows.Any(e => e.Id == id);
        }
    }
}
